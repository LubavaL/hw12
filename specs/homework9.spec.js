import { expect, test } from "@jest/globals";
import { apiProvider } from "../framework";
import { mailboxlayerAccessKey } from "../framework/config";

test("Проверяем корректный email", async () => {
  const { body, status } = await apiProvider().emailValidation().get(mailboxlayerAccessKey, "test@qa.ru");
  expect(status).toBe(200);
  expect(body.format_valid).toBe(true);
});

test.each`
  mail             | expected
  ${'12345'}       | ${false}
  ${'.test@qa.ru'} | ${false}
  ${'test@qa'}     | ${false}
`('Невалидное значение email = $mail', async ({ mail, expected }) => {
  const { body, status } = await apiProvider().emailValidation().get(mailboxlayerAccessKey, mail);
  expect(status).toBe(200);
  expect(body.format_valid).toBe(expected);
});

test("Проверяем доступ без accessKey", async () => {
  const { body, status } = await apiProvider().emailValidation().get(null, "test@qa.ru");
  expect(status).toBe(200);
  expect(body.success).toBe(false);
  expect(body.error.type).toBe("invalid_access_key");
});
