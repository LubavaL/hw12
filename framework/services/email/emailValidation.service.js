import supertest from 'supertest';
import { urls } from '../../config';

const Validation = function Validation() {
  this.get = async function get(accessKey, mail) {
    const r = await supertest(urls.mailboxlayer).get(`/api/check?access_key=${accessKey}&email=${mail}`);
    return r;
  };
};

export { Validation };