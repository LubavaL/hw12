import { Registration, Validation } from './services/index';


const apiProvider = () => ({
  emailValidation: () => new Validation(),
});

export { apiProvider };
